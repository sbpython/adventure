from adventure.games.the_meetup import the_meetup
from adventure.games.house import house

# to add a new adventure, import it above and add it to the list below

all_games = [
    the_meetup,
    house,
]
