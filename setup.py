from setuptools import setup, find_packages

setup(
    name='adventure',

    # We use a serial release numbering pattern. So the version is just
    # a whole number that increments by one on each release.
    version='1',
    packages=find_packages(),
    url='https://bitbucket.org/sbpython/adventure',
    license='LICENSE.txt',
    author='Chloe Beelby',
    author_email='coolbreezechloe@gmail.com',
    description='A text adventure game developed as a learning exercise at '
                'the south bend python meetup',
    python_requires='>=3',
    entry_points={
        'console_scripts': [
            'adventure=adventure.core:main'
        ]
    }
)

